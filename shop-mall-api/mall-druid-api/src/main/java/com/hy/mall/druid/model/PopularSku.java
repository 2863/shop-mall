package com.hy.mall.druid.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class PopularSku {

    private LocalDateTime accessTime;

    private String ip;

    private String uri;
}
