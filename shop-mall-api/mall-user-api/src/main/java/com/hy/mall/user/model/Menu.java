package com.hy.mall.user.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.hy.mall.utils.model.Hierarchy;
import com.hy.mall.utils.model.SearchInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_menu")
public class Menu extends Hierarchy<Menu> implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    private String url;

    private Integer parentId;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Long creator;

    private Long updator;

    private Integer status;

    private String title;

    private String icon;

    private Integer hidden;

    private Integer level;
}
