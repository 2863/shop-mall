package com.hy.mall.goods.model;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hy.mall.utils.model.SearchInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-09-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Sku extends SearchInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private Integer price;

    private Integer num;

    private String image;

    private String images;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Long spuId;

    private Integer categoryId;

    private String categoryName;

    private Integer brandId;

    private String brandName;

    private String skuAttribute;

    private Integer status;

    @TableField(exist = false)
    private Spu spu;
}
