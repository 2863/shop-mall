package com.hy.mall.cart.model;


import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Cart {

    @Id
    private Long _id;

    private String userName;

    private String skuName;

    private Long skuId;
}
