package com.hy.mall.order.model;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Kevin
 * @since 2021-09-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String userName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String shippingAddress;

    private String phone;

    private String contacts;

    private LocalDateTime shippingTime;

    private LocalDateTime finishTime;

    private Integer totalNum;

    private BigDecimal totalMoney;

    @TableField(exist = false)
    private List<Long> cartIds;
}
