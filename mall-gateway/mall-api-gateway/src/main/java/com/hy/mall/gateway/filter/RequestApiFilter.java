package com.hy.mall.gateway.filter;

import com.hy.mall.gateway.hot.HotQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static io.netty.handler.codec.http.cookie.CookieHeaderNames.MAX_AGE;

@Configuration
public class RequestApiFilter implements GlobalFilter, Ordered {

    @Autowired
    private HotQueue hotQueue;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        MultiValueMap<String, String> multiValueMap = request.getQueryParams();
        String account = "kevin";
        RequestPath path = request.getPath();
        String realPath = path.value();

        //除了登陆接口，其他接口都要拦截
        if(!"/admin/login".equals(realPath)){
            //校验是否有权限访问
        }
        //如果是抢购请求，让它去排队
        if("/seckill/orderQueue".equals(realPath)){
            //商品id
            Long skuId = Long.valueOf(multiValueMap.getFirst("skuId"));
            //购买数量
            Integer num = Integer.valueOf(multiValueMap.getFirst("num"));
           hotQueue.queue(account,skuId,num);
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 10003;
    }

    @Override
    public String toString() {
        return "RequestApiFilter{" +
                "hotQueue=" + hotQueue +
                '}';
    }
}
