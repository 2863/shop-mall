package com.hy.mall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MallApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallApiGatewayApplication.class, args);
    }

}
