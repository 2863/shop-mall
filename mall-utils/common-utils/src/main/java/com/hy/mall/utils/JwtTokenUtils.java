package com.hy.mall.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.auth0.jwt.interfaces.Verification;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class JwtTokenUtils {


    private static final String DEFAULT_SECRET = "hy.com";

    /**
     * 半个小时失效
     */
    private static final long EXPIRE_TIME_IN_MILLIS=360000;

    public static String createToken(Map<String,Object> claim){
        return createToken(DEFAULT_SECRET,claim);
    }

    public static String createToken(String secret, Map<String,Object> claim){
        if(StringUtils.isEmpty(secret)){
            secret = DEFAULT_SECRET;
        }
        Algorithm algorithm = Algorithm.HMAC256(secret);
       String token = JWT.create().withClaim("claim",claim) //数据有效载荷
                .withIssuer("hy") //签发人
                .withIssuedAt(new Date()) //签发时间
                .withExpiresAt(new Date(System.currentTimeMillis()+EXPIRE_TIME_IN_MILLIS)) //半个小时后失效
//                .withNotBefore(new Date(System.currentTimeMillis())) //1秒钟后起效
                .sign(algorithm);
       return token;
    }

    public static Map<String,Object> parseToken(String token){
        return parseToken(DEFAULT_SECRET,token);
    }
    public static Map<String,Object> parseToken(String secret,String token){
        if(StringUtils.isEmpty(secret)){
            secret = DEFAULT_SECRET;
        }
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWTVerifier verifier = JWT.require(algorithm).withIssuer("hy").build();
        DecodedJWT jwt = verifier.verify(token);
        return jwt.getClaim("claim").asMap();
    }

    public static void main(String[] args) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        map.put("name","kevin");
        map.put("company","hy");
        String token = JwtTokenUtils.createToken(null,map);
        System.out.println(token);
//        TimeUnit.SECONDS.sleep(1);
        try {
            Map map1 = JwtTokenUtils.parseToken(null, token);
            System.out.println(map1);
        }catch (Exception e){
            System.out.println("decode exception:");
            e.printStackTrace();
        }

    }
}
