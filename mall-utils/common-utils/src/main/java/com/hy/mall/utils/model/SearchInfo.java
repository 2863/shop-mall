package com.hy.mall.utils.model;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class SearchInfo {

    @TableField(exist = false)
    private long pageNum;

    @TableField(exist = false)
    private long pageSize;

    @TableField(exist = false)
    private String keyword;

}
