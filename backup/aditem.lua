-- 数据相应类型
ngx.header.content_type="application/json;charset=utf8"
local redis = require("resty.redis")

local categoryId = ngx.req.get_uri_args()["categoryId"]

local key = "ad_sku_list::"..categoryId

local red_ins = redis:new()

red_ins:set_timeout(2000)

red_ins:connect("192.168.1.21",6379)

local result = red_ins:get(key)

red_ins:close()

if result == nil or result == null or result == ngx.null then
 return true
else
 ngx.say(result)
end