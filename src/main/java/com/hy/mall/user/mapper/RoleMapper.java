package com.hy.mall.user.mapper;

import com.hy.mall.user.model.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
public interface RoleMapper extends BaseMapper<Role> {

}
