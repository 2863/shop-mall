package com.hy.mall.user.service;

import com.hy.mall.user.model.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
public interface UserRoleService extends IService<UserRole> {

}
