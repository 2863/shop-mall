package com.hy.mall.user.service.impl;

import com.hy.mall.user.model.RoleMenu;
import com.hy.mall.user.mapper.RoleMenuMapper;
import com.hy.mall.user.service.RoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
