package com.hy.mall.user.service;

import com.hy.mall.user.model.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
public interface RoleService extends IService<Role> {

}
