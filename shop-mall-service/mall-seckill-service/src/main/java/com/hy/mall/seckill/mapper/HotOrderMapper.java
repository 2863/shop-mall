package com.hy.mall.seckill.mapper;

import com.hy.mall.seckill.model.HotOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-11-13
 */
public interface HotOrderMapper extends BaseMapper<HotOrder> {

}
