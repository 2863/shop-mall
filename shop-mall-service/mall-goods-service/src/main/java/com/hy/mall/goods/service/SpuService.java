package com.hy.mall.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hy.mall.goods.model.Spu;

public interface SpuService extends IService<Spu> {
    void addSpu(Spu spu);
}
