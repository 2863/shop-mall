package com.hy.mall.goods.mapper;

import com.hy.mall.goods.model.Spu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-09-25
 */
public interface SpuMapper extends BaseMapper<Spu> {

}
