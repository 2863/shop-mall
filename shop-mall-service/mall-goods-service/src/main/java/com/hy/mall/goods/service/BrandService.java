package com.hy.mall.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hy.mall.goods.model.Brand;

public interface BrandService extends IService<Brand> {
}
