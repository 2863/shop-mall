package com.hy.mall.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hy.mall.exception.InsufficientStockException;
import com.hy.mall.goods.model.Sku;

import java.util.List;

public interface SkuService extends IService<Sku> {
    int deductStock(Long skuId, int stock) throws InsufficientStockException;

    List<Sku> skuListByCategoryId(Integer categoryId);

    void removeByCategoryId(Integer categoryId);

    List<Sku> updateByCategoryId(Integer categoryId);
}
