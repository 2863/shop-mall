package com.hy.mall.druid.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hy.mall.druid.mapper.PopularSkuMapper;
import com.hy.mall.druid.model.PopularSku;
import org.springframework.stereotype.Service;

@Service
public class PopularSkuServiceImpl extends ServiceImpl<PopularSkuMapper, PopularSku> {

}
