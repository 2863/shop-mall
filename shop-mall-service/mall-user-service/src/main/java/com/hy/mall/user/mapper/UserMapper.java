package com.hy.mall.user.mapper;

import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.hy.mall.user.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
public interface UserMapper extends BaseMapper<User> {

    @Select("select r.* from tb_role r inner join tb_user_role ur on ur.role_id=r.id where ur.user_id=#{userId}")
    List<Role> selectUserRoles(Long userId);

    @Select("select m.* from tb_menu m inner join tb_role_menu rm on rm.menu_id=m.id" +
            " inner join tb_user_role ur on ur.role_id=rm.role_id where ur.user_id=#{userId}")
    List<Menu> selectUserMenus(Long userId);
}
