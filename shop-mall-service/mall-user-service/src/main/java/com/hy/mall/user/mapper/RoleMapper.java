package com.hy.mall.user.mapper;

import com.hy.mall.user.model.Menu;
import com.hy.mall.user.model.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-12-22
 */
public interface RoleMapper extends BaseMapper<Role> {

    @Update("update tb_role set status=#{status} where id=#{id}")
    int updateStatus(@Param("id") Integer id, @Param("status") Integer status);

    @Select("select m.* from tb_menu m inner join tb_role_menu rm on rm.menu_id=m.id where rm.role_id=#{roleId}")
    List<Menu> selectRoleMenus(Integer roleId);
}
