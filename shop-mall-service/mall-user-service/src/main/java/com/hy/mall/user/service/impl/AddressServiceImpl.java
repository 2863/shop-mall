package com.hy.mall.user.service.impl;

import com.hy.mall.user.model.Address;
import com.hy.mall.user.mapper.AddressMapper;
import com.hy.mall.user.service.AddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-11-12
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements AddressService {

}
