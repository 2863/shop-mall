package com.hy.mall.order.service;

import com.hy.mall.order.model.Item;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-09-28
 */
public interface ItemService extends IService<Item> {

}
