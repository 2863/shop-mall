package com.hy.mall.order.service.impl;

import com.hy.mall.order.model.Item;
import com.hy.mall.order.mapper.ItemMapper;
import com.hy.mall.order.service.ItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-09-28
 */
@Service
public class ItemServiceImpl extends ServiceImpl<ItemMapper, Item> implements ItemService {

}
