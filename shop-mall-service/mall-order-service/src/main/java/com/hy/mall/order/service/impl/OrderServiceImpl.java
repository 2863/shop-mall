package com.hy.mall.order.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.hy.mall.cart.feign.CartFeign;
import com.hy.mall.cart.model.Cart;
import com.hy.mall.exception.InsufficientStockException;
import com.hy.mall.goods.feign.GoodsFeign;
import com.hy.mall.order.mapper.ItemMapper;
import com.hy.mall.order.model.Item;
import com.hy.mall.order.model.Order;
import com.hy.mall.order.mapper.OrderMapper;
import com.hy.mall.order.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hy.mall.utils.Result;
import com.hy.mall.utils.StatusCode;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-09-28
 */
@Slf4j
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {


    @Autowired
    private ItemMapper itemMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private CartFeign cartFeign;

    @Autowired
    private GoodsFeign goodsFeign;

    @GlobalTransactional
    @Override
    public void createOrder(String userName, Order order){

        Long orderId = IdWorker.getId();
        order.setId(orderId);
        order.setUserName(userName);
        order.setCreateTime(LocalDateTime.now());
        order.setUpdateTime(LocalDateTime.now());

        orderMapper.insert(order);
        Result<Integer> deductResult = goodsFeign.deductStock(1457685871500312578L,10);
        if(!StatusCode.SUCCESS.equals(deductResult.getCode())){
            throw new InsufficientStockException("扣减库存失败");
        }
        /*//1、查询购物车数据是否存在
        List<Cart> cartList = cartFeign.getCartList(order.getCartIds());
        if(cartList==null||cartList.isEmpty()){
            return;
        }
        //购物车商品和查询的商品数量不一致
        if(cartList.size()!=order.getCartIds().size()){
            log.info("购物车中的商品与数据库中商品");
            return;
        }
        //2、扣减库存
        for (Cart cart : cartList) {
            goodsFeign.deductStock(cart.getSkuId(),10);
        }


        //3、添加商品明细
        List<Item> itemList = itemMapper.selectBatchIds(null);
        for (Item item : itemList) {

            //设置订单详情表与订单表关系
            item.setOrderId(order.getId());
            //保存商品明细对象
            itemMapper.insert(item);
        }

        //4、保存订单数据
        getBaseMapper().insert(order);
        //5、清空购物车
        cartFeign.clearOrderedCartInfo(order.getCartIds());*/
    }
}
